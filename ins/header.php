<!DOCTYPE html>
<html>
<head>
	<title>OOP Login-Registration System</title>
	<link rel="stylesheet" href="ins/bootstrap.min.css">
	<script src="ins/jquery.min.js"></script>
	<script src="ins/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="index">PHP OOP</a>
				</div>
				<ul class="nav navbar-nav pull-right">
					<li><a href="profile.php">Profile</a></li>
					<li><a href="login.php">Login</a></li>
					<li><a href="signup.php">Signup</a></li>
					
				</ul>
			</div>
		</nav>