<?php
/**
* 
*/
class Database{
	private $hostdb="localhost";
	private $userdb="root";
	private $namedb="oop_lr";
	private $passdb="";
	public $pdo;
	function __construct()
	{
		if (!isset($this->pdo)) {
			
			try{
				$link= new PDO("mysql:host=".$this->hostdb.";dbname =".$this->namedb,$this->passdb,$this->userdb);
				$link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$link->exec("SET CHARACTER SET utf8");
				$this->pdo=$link;
			}
			catch(PDOException $e){
				die("Failed to connect with database".$e->getMessage());
			}
		}
	}
}

?>