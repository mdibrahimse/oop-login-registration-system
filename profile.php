<?php include 'ins/header.php'; ?>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h2>User Profile<span class="pull-right"><a href="index.php" class="btn btn-primary">Back</a></span></h2>
			</div>
			<div class="panel-body">
				<div style="max-width: 400px; margin:0 auto">
				<form action="" method="post">
					<div class="form-group">
						<label for="name">Full Name</label>
						<input type="text" name="name" id="name" class="form-control" required="">
					</div>
					<div class="form-group">
						<label for="email">Email Address</label>
						<input type="text" name="email" id="email" class="form-control" required="">
					</div>
					<div class="form-group">
						<label for="user_name">User Name</label>
						<input type="text" name="user_name" id="user_name" class="form-control" required="">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" name="password" id="password" class="form-control" required="">
					</div>
					<button type="submit" name="update" class="btn btn-success">Update</button>
				</form>
				</div>
			</div>
		</div>

<?php include 'ins/footer.php'; ?>

